ESX = nil

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
end)

POWX_Shops_Config = {
    Positions = {
        -- Superette
	{name = 'Superette', x = 25.742,    y = -1345.741, z = 29.57},
    {name = 'Superette', x = 373.875,   y = 325.896,  z = 103.66},
	{name = 'Superette', x = 2557.458,  y = 382.282,  z = 108.722}, 
	{name = 'Superette', x = -3038.939, y = 585.954,  z = 7.97},
	{name = 'Superette', x = -3241.927, y = 1001.462, z = 12.850}, 
	{name = 'Superette', x = 547.431,   y = 2671.710, z = 42.176}, 
	{name = 'Superette', x = 1961.464,  y = 3740.672, z = 32.363}, 
	{name = 'Superette', x = 2678.916,  y = 3280.671, z = 55.261}, 
    {name = 'Superette', x = 1729.216,  y = 6414.131, z = 35.057}, 
    {name = 'Superette', x = 1135.808,  y = -982.281,  z = 46.45}, 
	{name = 'Superette', x = -1222.93,  y = -906.99,  z = 12.35}, 
	{name = 'Superette', x = -1487.553, y = -379.107,  z = 40.163}, 
	{name = 'Superette', x = -2968.243, y = 390.910,   z = 15.054}, 
	{name = 'Superette', x = 1166.024,  y = 2708.930,  z = 38.167}, 
	{name = 'Superette', x = 1392.562,  y = 3604.684,  z = 34.995}, 
    {name = 'Superette', x = -1393.409, y = -606.624,  z = 30.319}, 
    {name = 'Superette', x = -1037.618,  y = -2737.399,   z = 20.169}, 
    {name = 'Superette', x = -48.519,   y = -1757.514, z = 29.47}, 
    {name = 'Superette', x = 1163.373,  y = -323.801,  z = 69.27}, 
	{name = 'Superette', x = -707.67,  y = -914.22,  z = 19.26}, 
	{name = 'Superette', x = -1820.523, y = 792.518,   z = 138.20},
	{name = 'Superette', x = 1698.388,  y = 4924.404,  z = 42.083}
	},
	
	

    Items = {
        {Label = '🥖  Pain', Value = 'bread', Price = 5},
		{Label = '🍔  Hamburger', Value = 'hamburger', Price = 12},
		{Label = '💧  Eau', Value = 'water', Price = 5}
    }
}

Citizen.CreateThread(function()
	for k, v in pairs(POWX_Shops_Config.Positions) do
		local blip = AddBlipForCoord(v.x, v.y, v.z)

		SetBlipSprite(blip, 52)
		SetBlipScale (blip, 0.8)
		SetBlipColour(blip, 2)
		SetBlipAsShortRange(blip, true)

		BeginTextCommandSetBlipName('STRING')
		AddTextComponentSubstringPlayerName(_U('shop') .. ' ['.. v.name .. ']')
		EndTextCommandSetBlipName(blip)
	end
end)

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(0)
		local playerCoords = GetEntityCoords(PlayerPedId())

		for k, v in pairs(POWX_Shops_Config.Positions) do
			local distance = GetDistanceBetweenCoords(playerCoords, v.x, v.y, v.z, true)

            if distance < 10.0 then
                actualZone = v

                zoneDistance = GetDistanceBetweenCoords(playerCoords, actualZone.x, actualZone.y, actualZone.z, true)

				DrawMarker(29, v.x, v.y, v.z, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 16, 255, 40, 100, false, true, 2, false, nil, nil, false)
            end
            
            if distance <= 1.5 then
                ESX.ShowHelpNotification(_U('open_menu'))

                if IsControlJustPressed(1, 51) then
                    RageUI.Visible(RMenu:Get('showcase', 'shopMenu'), not RageUI.Visible(RMenu:Get('showcase', 'shopMenu')))
                end
            end

            if zoneDistance ~= nil then
                if zoneDistance > 1.5 then
                    RageUI.CloseAll()
                end
            end
		end
	end
end)

local max = 15 
Numbers = {}

Citizen.CreateThread(function()
    for i = 1, max do
        table.insert(Numbers, i)
    end
end)