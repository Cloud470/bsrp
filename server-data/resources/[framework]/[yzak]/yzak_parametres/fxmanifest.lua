fx_version 'adamant'
games {'gta5'}

description 'YZAK PRM'

-- HTML Mode Cinematique

ui_page 'html/ui.html'

files {
    'html/ui.html',
    'html/img/image.png',
    'html/css/app.css',
    'html/scripts/app.js'
}

-- RageUI
client_scripts {
    "src/RMenu.lua",
    "src/menu/RageUI.lua",
    "src/menu/Menu.lua",
    "src/menu/MenuController.lua",

    "src/components/*.lua",

    "src/menu/elements/*.lua",

    "src/menu/items/*.lua",

    "src/menu/panels/*.lua",

    "src/menu/panels/*.lua",
    "src/menu/windows/*.lua",
}

client_scripts {
    'cl_parametres.lua'
}

----------------------------------------
------------ Créé By PowX ------------
----------------------------------------