INTERIORS = {
    -- HOPITAL
    [1] = {id = 1, x = 275.46, y = -1361.22, z = 24.53, h = 50.37, name = "Hopîtal", destination = {2,3}},
    [2] = {id = 2, x = 343.47, y = -1398.81, z = 32.51, h = 49.13, name = "Sortie principale", destination = {1,3}},
    [3] = {id = 3, x = 335.16, y = -1432.06, z = 46.51, h = 141.75, name = "Sortie de service", destination = {1,2}},

    -- HUMANE LABS
    [4] = {id = 10, x = 3540.859375, y = 3675.7958984375, z = 28.121143341064, h = 166.72660827637, name = "Etage n°-1", destination = {5}},
    [5] = {id = 11, x = 3540.8566894531, y = 3676.0424804688, z = 20.991781234741, h = 173.42085266113, name = "Etage n°-3", destination = {4}},

    -- CRACK TRAITEMENT
--[[    [6] = {id = 6, x = 595.815, y = -457.35, z = 24.84, h = 171.165451, name = "Porte traitement crack", destination = {7,6}},
    [7] = {id = 7, x = 1089, y = -3188, z = 39, h = 12.145, name = "Porte traitement crack", destination = {6,7}},--]]
}
