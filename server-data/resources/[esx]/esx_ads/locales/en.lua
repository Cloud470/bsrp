Locales ['en'] = {
  ['you_paid'] = 'Vous payez ~g~$%s~s~ pour votre pub.',
  ['you_paid_black'] = 'Vous payez ~r~$%s~s~ pour votre message sur le DarkNet.',
  ['not_enough_money'] = 'Vous ~r~n\'avez~w~ pas assez d\'argent.',
  ['no_job'] = 'Vous ~r~n\'avez~w~ pas accès au système de publicité.',
  ['no_password'] = 'Vous ~r~n\'avez~w~ pas de ~r~mot de passe~w~ pour utiliser ce service.',
  ['ad_name'] = 'Publicité.',
}