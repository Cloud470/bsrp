Locales ['en'] = {
	['used_kit']					= 'Vous avez utilisé lockpick.',
	['must_be_outside']				= 'You must be outside of the vehicle!',
	['no_vehicle_nearby']			= 'There is no vehicle nearby.',
	['vehicle_unlocked']			= 'Vehicle is now unlocked',
	['unjammed_handbrake']			= 'Frein à main enlvé',
	['lockpicked_successful']	    = 'Crochetage réussi!',
	['abort_hint']					= 'Press ~INPUTGROUP_MOVE~ to cancel.',
	['aborted_lockpicking']		    = 'You aborted the lockpicking.',
	['picklock_failed']		        = 'Crochetage raté.',
	['911Call']		                = '911 Call.',
	['911Lockpick']		            = 'Agréssion',
	['Lockcall']		            = 'Vol de voiture en cours.',
}
