Locales['fr'] = {
  -- Cloakroom
  ['cloakroom'] = 'Vestiaire',
  ['ems_clothes_civil'] = 'Tenue Civil',
  ['ems_clothes_ems'] = 'Tenue Ambulancier',
  -- Vehicles
  ['veh_menu'] = 'véhicule',
  ['veh_spawn'] = 'appuyez sur ~INPUT_CONTEXT~ pour sortir un véhicule',
  ['store_veh'] = 'appuyez sur ~INPUT_CONTEXT~ pour ranger le véhicule',
  ['ambulance'] = 'ambulance',
  ['helicopter'] = 'hélicoptère',
  -- Action Menu
  ['hospital'] = 'hôpital',
  ['revive_inprogress'] = 'réanimation en cours',
  ['revive_complete'] = 'vous avez réanimé ~y~%s~s~',
  ['revive_complete_award'] = 'vous avez réanimé ~y~%s~s~, ~g~$%s~s~',
  ['heal_inprogress'] = 'you are healing!',
  ['heal_complete'] = 'vous avez soigné ~y~%s~s~',
  ['no_players'] = 'aucun joueur à proximité',
  ['no_vehicles'] = 'aucun véhicule à proximité',
  ['player_not_unconscious'] = 'n\'est pas inconscient',
  ['player_not_conscious'] = 'Cette personne est inconsciente!',
  -- Boss Menu
  ['deposit_society'] = 'déposer argent',
  ['withdraw_society'] = 'retirer argent société',
  ['boss_actions'] = 'action Patron',
  -- Misc
  ['invalid_amount'] = '~r~montant invalide',
  ['open_menu'] = 'appuyez sur ~INPUT_CONTEXT~ pour ouvrir le menu',
  ['deposit_amount'] = 'montant du dépôt',
  ['money_withdraw'] = 'montant du retrait',
  ['fast_travel'] = 'appuyez sur ~INPUT_CONTEXT~ pour vous déplacer rapidement.',
  ['open_pharmacy'] = 'appuyez sur ~INPUT_CONTEXT~ pour ouvrir la pharmacie.',
  ['pharmacy_menu_title'] = 'Pharmacie',
  ['pharmacy_take'] = 'Prendre',
  ['medikit'] = 'kit de soin',
  ['bandage'] = 'bandage',
  ['max_item'] = 'vous en portez déjà assez sur vous.',
  -- F6 Menu
  ['ems_menu'] = 'interaction citoyen',
  ['ems_menu_title'] = 'ambulance - Interactions Citoyen',
  ['ems_menu_revive'] = 'réanimer',
  ['ems_menu_putincar'] = 'mettre dans véhicule',
  ['ems_menu_small'] = 'soigner petites blessures',
  ['ems_menu_big'] = 'soigner blessures graves',
  -- Phone
  ['alert_ambulance'] = 'alerte Ambulance',
  -- Death
  ['please_wait'] = 'réanimation possible dans ~b~%s minutes %s seconds~s~\n',
  ['press_respawn'] = 'appuyez sur [~b~E~w~] pour être réanimé',
  ['respawn_now_fine'] = 'pour être réanimé pour ~g~$%s maintenez ~w~[Press ~b~E~w~]',
  ['respawn_fine'] = 'vous avez payer ~r~$%s~s~ pour être réanimé',
  ['distress_send'] = 'appuyez sur [~b~G~s~] pour envoyer un signal de détresse',
  ['distress_sent'] = 'un signal a été envoyé à toutes les unités disponibles !',
  ['distress_message'] = 'interventtion requise: citoyen inconscient !',
  -- Revive
  ['revive_help'] = 'relancer un joueur',
  -- Item
  ['used_medikit'] = 'vous avez utilisé 1x kit de soin',
  ['used_bandage'] = 'vous avez utilisé 1x bandage',
  ['not_enough_medikit'] = 'vous n\'avez pas de ~b~kit de soin~w~.',
  ['not_enough_bandage'] = 'vous n\'avez pas de ~b~bandage~w~.',
  ['healed'] = 'vous avez été soigné.',
  ['billing'] = 'Facture',
  ['invoice_amount'] = 'Montant de la facture',
  ['amount_invalid'] = 'montant invalide',
  ['billing_sent'] = 'Facture envoyé !',
  -- Misc
}
